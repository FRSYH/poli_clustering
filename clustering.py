from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
import pandas as pd
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn import preprocessing
from pandas import ExcelWriter
import math

#---------------------------------------------------------------
def replace_zero_with_featrue_mean(X, start=1):
	features_mean = np.nanmean(X, axis=0)
	#print(features_mean)
	for row in range(X.shape[0]):
		for col in range(start, X.shape[1]):
			if X[row][col] == 0: 
				X[row][col] = features_mean[col]
	return X
#---------------------------------------------------------------
def elbow_method(X):
	max = 20
	wcss = []

	for i in range (1,max):
	    kmeans = KMeans(n_clusters = i, init='k-means++', random_state=0) 
	    kmeans.fit(X)
	    wcss.append(kmeans.inertia_)

	plt.plot(range(1,max),wcss)
	plt.title('Elbow Method')
	plt.xlabel('Number of clusters')
	plt.ylabel('wcss')
	plt.show()

#----------------------------------------------------------------
def nan_percentage(X):
	nan = 0
	for index,val in np.ndenumerate(X):
		if np.isnan(val):
			nan+=1
	total = X.shape[0] * X.shape[1]
	print("NAN percentage =",(nan/total*100), "%")


#----------------------------------------------------------------
def print_cluster(X, labels):

	clusters = {}
	n = 0
	for item in labels:
	    if item in clusters:
	        clusters[item].append(X[n])
	    else:
	        clusters[item] = [X[n]]
	    n +=1


	for item in clusters:
	    print("Cluster ", item)
	    for i in clusters[item]:
	        print(i)     


#------------------------------------------------------------------------
#FROM https://stackoverflow.com/questions/35611465/python-scikit-learn-clustering-with-missing-data
def kmeans_missing(X, n_clusters, max_iter=10):
    """Perform K-Means clustering on data with missing values.

    Args:
      X: An [n_samples, n_features] array of data to cluster.
      n_clusters: Number of clusters to form.
      max_iter: Maximum number of EM iterations to perform.

    Returns:
      labels: An [n_samples] vector of integer labels.
      centroids: An [n_clusters, n_features] array of cluster centroids.
      X_hat: Copy of X with the missing values filled in.
    """

    # Initialize missing values to their column means
    missing = ~np.isfinite(X)
    mu = np.nanmean(X, 0, keepdims=1)
    X_hat = np.where(missing, mu, X)

    for i in xrange(max_iter):
        if i > 0:
            # initialize KMeans with the previous set of centroids. this is much
            # faster and makes it easier to check convergence (since labels
            # won't be permuted on every iteration), but might be more prone to
            # getting stuck in local minima.
            cls = KMeans(n_clusters, init=prev_centroids)
        else:
            # do multiple random initializations in parallel
            cls = KMeans(n_clusters, n_jobs=-1)

        # perform clustering on the filled-in data
        labels = cls.fit_predict(X_hat)
        centroids = cls.cluster_centers_

        # fill in the missing values based on their cluster centroids
        X_hat[missing] = centroids[labels][missing]

        # when the labels have stopped changing then we have converged
        if i > 0 and np.all(labels == prev_labels):
            break

        prev_labels = labels
        prev_centroids = cls.cluster_centers_

    return labels, centroids, X_hat

def visualize_cluster(X,labels):
	plt.title("K means") 
	plt.scatter(X[:,0],X[:,1], c=labels, cmap='rainbow')
	#kmeans centroids
	plt.scatter(kmeans.cluster_centers_[:,0] ,kmeans.cluster_centers_[:,1], color='black')
	plt.show()


def cluster_mean(X, labels, n_cluster):

	clusters = {}
	n = 0
	for item in labels:
	    if item in clusters:
	        clusters[item].append(X[n])
	    else:
	        clusters[item] = [X[n]]
	    n +=1

	row, col = X.shape
	#print row, col
	
	cluster_means = np.zeros((n_cluster, col))


	for feature in range(0,col):
		#print "Feature ", feature
		for index in clusters:
			#print "Cluster ", index    
			mean = 0
			sum = 0
			row = len(clusters[index])
			for observation in clusters[index]:
				sum += observation[feature]
			mean = sum/row
			sum = 0
			cluster_means[index,feature] = mean

	#print cluster_means
	return cluster_means		

def cluster_variance(X, labels, n_cluster):



	clusters = {}
	n = 0
	for item in labels:
	    if item in clusters:
	        clusters[item].append(X[n])
	    else:
	        clusters[item] = [X[n]]
	    n +=1

	row, col = X.shape
	#print row, col
	
	cluster_means = cluster_mean(X, labels, n_cluster)
	cluster_variance = np.zeros((n_cluster, col))

	for feature in range(0,col):
		#print "Feature ", feature
		for index in clusters:
			#print "Cluster ", index    
			var = 0
			sum = 0
			row = len(clusters[index])
			for observation in clusters[index]:
				sum += np.power((observation[feature] - cluster_means[index,feature]), 2)	
			var = sum/row
			sum = 0
			cluster_variance[index,feature] = var

	#print cluster_means
	return cluster_variance	

def cluster_standard_deviation(X, labels, n_cluster):

	clusters = {}
	n = 0
	for item in labels:
	    if item in clusters:
	        clusters[item].append(X[n])
	    else:
	        clusters[item] = [X[n]]
	    n +=1

	row, col = X.shape
	#print row, col
	
	cluster_means = cluster_mean(X, labels, n_cluster)
	cluster_standard_deviation = np.zeros((n_cluster, col))

	for feature in range(0,col):
		#print "Feature ", feature
		for index in clusters:
			#print "Cluster ", index    
			var = 0
			sum = 0
			row = len(clusters[index])
			for observation in clusters[index]:
				sum += np.power((observation[feature] - cluster_means[index,feature]), 2)	
			var = sum/row
			sum = 0
			cluster_standard_deviation[index,feature] = math.sqrt(var)

	#print cluster_means
	return cluster_standard_deviation

def cluster_coefficient_of_variation(means, standard_deviation, k):

	col = len(means[0])
	coefficient = np.zeros((k, col))
	for cluster in range(k):
		for feature in range(col):
			if means[cluster][feature] != 0:
				coefficient[cluster][feature] = standard_deviation[cluster][feature] / means[cluster][feature]
			else:
				coefficient[cluster][feature] = 0
	return coefficient

def bar_plot(data, description):

	row, col = data.shape
	ind = np.arange(col)  # the x locations for the groups
	width = 0.25  # the width of the bars

	colors = ["SkyBlue","IndianRed", "MediumAquaMarine", "Orchid", "Gold"]

	pos = list(range(col)) 

	fig, ax = plt.subplots()
	#rects1 = ax.bar(ind - width/2, means[0], width, color=colors[0], label='Men')
	#rects2 = ax.bar(ind + width/2, means[1], width, color=colors[1], label='Women')
	x = ind - width/row
	for cluster in range(0,row):
		lab = "Cluster "
		lab += str(cluster)
		ax.bar([p + (width*cluster) for p in pos], data[cluster], width, color=colors[cluster], label=lab)
		x = ind + (cluster+1)*width/row
	
	# Add some text for labels, title and custom x-axis tick labels, etc.
	if kind == 1:
		subject = 'Donors, '
	if kind == 2:
		subject = 'Not donors, '	
	if kind == 3:
		subject = 'All observations, '	

	title = subject + str(description)
	ylabel = description
	ax.set_ylabel(ylabel)
	ax.set_title(title)
	ax.set_xticks([p + (0.5*(row-1))*width for p in pos])

	if nuovo_input == 0:
		if kind == 1:
			ax.set_xticklabels(('D3,1', 'D3,2', 'D3,3', 'D3,4', 'D3,5', 'D3,6', 'D3,7', 'D3,8', 'D3,9', 'D3,10', 'D3,11', 'D3,12', 'D5', 'D6', 'D7', 'D8', 'D9', 'D10', 'D11' ))
		if kind == 2:
			ax.set_xticklabels(('D4,1', 'D4,2', 'D4,3', 'D4,4', 'D4,5', 'D5', 'D6', 'D7', 'D8', 'D9', 'D10', 'D11' ))
		if kind == 3:
			ax.set_xticklabels(('D5', 'D6', 'D7', 'D8', 'D9', 'D10', 'D11' ))
	else:
		if kind == 1:
			ax.set_xticklabels(('Environment', 'Social Walfare', 'Religious', 'Health',
			 'Emotional', 'Empowerment', 'Morals', 'Eco benefits', 'Fashion', 'D5', 'D6', 'D7', 'D8', 'D9', 'D10', 'D11' ), rotation=55)

#			ax.set_xticklabels(('Environment', 'Social Walfare', 'Religious Cause', 'Health and Research',
#			'D3,1', 'D3,2', 'D3,3', 'D3,4', 'D3,5', 'D3,6', 'D3,7', 'D3,8', 'D3,9', 'D3,10', 'D3,11', 'D3,12',
#			 'D5', 'D6', 'D7', 'D8', 'D9', 'D10', 'D11' ), rotation=55)


		if kind == 2:
			ax.set_xticklabels(('Lack of funds', 'Distrustful', 'Passive', 'D5', 'D6', 'D7', 'D8', 'D9', 'D10', 'D11'))
		if kind == 3:
			ax.set_xticklabels(('D5', 'D6', 'D7', 'D8', 'D9', 'D10', 'D11' ))



	ax.legend()
	plt.xlim(min(pos)-width, max(pos)+width*4)
	if description == 'standard deviation':
		plt.ylim(0,6)
	plt.show()

	#incremento di 0.5 per ogni cluster#

def scale_range (input, min, max):
    input += -(np.min(input))
    input /= np.max(input) / (max - min)
    input += min
    return input


def excel_output():

	specific_data = data_csv.loc[data_csv[9] == kind ].copy()
	if nuovo_input == 0:
		specific_data[43] = labels
	else:
		specific_data[47] = labels

	path = ''
	if kind == 1:
		path += 'donor/'
		path += 'K' + str(k) + '/'
	if kind == 2:
		path += 'not_donor/'
		path += 'K' + str(k) + '/'
	if kind == 3:
		path += ''

	file_name = 'output/' + path 


	if nuovo_input == 0:
		writer = pd.ExcelWriter(file_name + 'assigned_cluster.xlsx')
	else:
		writer = pd.ExcelWriter(file_name + 'new_assigned_cluster.xlsx')
	
	specific_data.to_excel(writer,'Sheet1')
	writer.save()

	if nuovo_input == 0:
		df = pd.read_excel(file_name + 'assigned_cluster.xlsx')
		writer = pd.ExcelWriter(file_name + 'clusters_values.xlsx')
	else:
		df = pd.read_excel(file_name + 'new_assigned_cluster.xlsx')
		writer = pd.ExcelWriter(file_name + 'new_clusters_values.xlsx')

	if nuovo_input == 0:
		if kind == 1:
			columns = ['D3,1', 'D3,2', 'D3,3', 'D3,4', 'D3,5', 'D3,6', 'D3,7', 'D3,8', 'D3,9', 'D3,10', 'D3,11', 'D3,12', 'D5', 'D6', 'D7', 'D8', 'D9', 'D10', 'D11' ]
		if kind == 2:
			columns = ['D4,1', 'D4,2', 'D4,3', 'D4,4', 'D4,5', 'D5', 'D6', 'D7', 'D8', 'D9', 'D10', 'D11']
		if kind == 3:
			columns = ['D5', 'D6', 'D7', 'D8', 'D9', 'D10', 'D11']		
	else:
		if kind == 1:
			columns = ['Environment', 'Social Walfare', 'Religious Cause', 'Health and Research',
			 'Emotional', 'Empowerment', 'Morals and traditional', 'Economi benefits', 'Fashion', 'D5', 'D6', 'D7', 'D8', 'D9', 'D10', 'D11' ]
#			columns = ['Environment', 'Social Walfare', 'Religious Cause', 'Health and Research',
#			'D3,1', 'D3,2', 'D3,3', 'D3,4', 'D3,5', 'D3,6', 'D3,7', 'D3,8', 'D3,9', 'D3,10', 'D3,11', 'D3,12',
#			 'D5', 'D6', 'D7', 'D8', 'D9', 'D10', 'D11' ]


		if kind == 2:
			columns = ['Lack of funds', 'Distrustful', 'Passive', 'D5', 'D6', 'D7', 'D8', 'D9', 'D10', 'D11']
		if kind == 3:
			columns = ['D5', 'D6', 'D7', 'D8', 'D9', 'D10', 'D11']		

	#value_by_cluster 
	row, col = X.shape
	clusters = {}
	n = 0
	for item in labels:
	    if item in clusters:
	        clusters[item].append(X[n])
	    else:
	        clusters[item] = [X[n]]
	    n +=1

	if nuovo_input == 0:
		id_index = 43
	else:
		id_index = 47

	for item in clusters:
		print("Cluster ", item)
		m = np.empty((0,col),int)
		ids = (df.loc[df[id_index] == item]).iloc[:,0]
		for i in clusters[item]:
			m = np.append(m,[i],axis=0)
		ids = list(ids)
		cluster_id = pd.DataFrame(data=m,columns=columns)
		cluster_id['ID'] = ids
		title = 'Cluster' + str(item)
		cluster_id.to_excel(writer,title)
	writer.save()


def graphics():

	#MEDIA
	#Compute the mean for every features
	cluster_means = cluster_mean(X,labels,k)
	cluster_means = np.abs(cluster_means)
	print("Cluster mean\n", cluster_means)
	#Display as a barplot the difference between every features
	bar_plot(cluster_means, 'mean')

	#VARIANZA
	print('\n\n\n\n')
	#Compute the mean for every features
	cluster_variances = cluster_variance(X,labels,k)
	print("Cluster variance\n",cluster_variances)
	#Display as a barplot the difference between every features
	bar_plot(cluster_variances, 'variance')

	#DEVIAZIONE STANDARD
	print('\n\n\n\n')
	#Compute the mean for every features
	cluster_standard_deviations = cluster_standard_deviation(X,labels,k)
	print("Cluster deviazione standard\n",cluster_standard_deviations)
	#Display as a barplot the difference between every features
	bar_plot(cluster_standard_deviations, 'standard deviation')	


	#COEFFICIENT OF VARIATION
	print('\n\n\n\n')
	coefficient = cluster_coefficient_of_variation(cluster_means, cluster_standard_deviations, k)
	print("Cluster coefficient of variation\n", coefficient)
	#Display as a barplot the difference between every features
	bar_plot(coefficient, 'coefficient of variation')	


'''---------------------------------------------------------------------'''
k = 2
#kind = donor(1) or not donor(2)
kind = 1
nuovo_input = 1


if nuovo_input == 0:
	data_csv = pd.read_csv('data/dataset.csv', header=None)
	if kind == 1:
		dataset = data_csv.loc[data_csv[9] == kind]
		data = dataset.iloc[:,18:42].fillna(0)
		X = np.array(data)
		X = replace_zero_with_featrue_mean(X)
		X = np.delete(X, [12,13,14,15,16], 1)
		print(data)

	if kind == 2:
		dataset = data_csv.loc[data_csv[9] == kind]
		data = dataset.iloc[:,30:42].fillna(0)
		X = np.array(data)
		X = replace_zero_with_featrue_mean(X)

	if kind == 3:
		data = data_csv.iloc[:,35:42].fillna(0)
		X = np.array(data)
		X = replace_zero_with_featrue_mean(X)	
else:
	data_csv = pd.read_csv('data/nuovo input clean.csv', header=None, sep=';')
	if kind == 1:
		col = [10,11,12,13,14,15,16,17,18,39,40,41,42,43,44,45]
		#col = [10,11,12,13,19,20,21,22,23,24,25,26,27,28,29,30,39,40,41,42,43,44,45]
		
		dataset = data_csv.loc[data_csv[9] == kind]
		data = dataset.iloc[:,col].fillna(0)		
		X = np.array(data)
		X = replace_zero_with_featrue_mean(X,9)
		for row in X:
			for index in range(0,4):
				if row[index] > 0:
					row[index] = 1

	if kind == 2:
		col = [31,32,33,39,40,41,42,43,44,45]
		dataset = data_csv.loc[data_csv[9] == kind]
		data = dataset.iloc[:,col].fillna(0)
		X = np.array(data)
		X = replace_zero_with_featrue_mean(X,4)

	if kind == 3:
		data = data_csv.iloc[:,35:42].fillna(0)
		X = np.array(data)
		X = replace_zero_with_featrue_mean(X)


std_scale = preprocessing.StandardScaler().fit(X)
X = std_scale.transform(X)

#Elbow mothod for finding best fitting k
#elbow_method(X)



#Compute kmeans for the correct k
kmeans= KMeans(n_clusters=k,init='k-means++', random_state=0)
labels = kmeans.fit_predict(X)
#print the cluster of each observation
print("Assigned Cluster")
#print(labels)

X = std_scale.inverse_transform(X)

print("\n\nValue by Cluster\n\n")
#print_cluster(X, labels)

excel_output()

graphics()





'''
std_scale = preprocessing.StandardScaler().fit(X)
X = std_scale.transform(X)

#PCA e VISUALIZZAZIONE 
Model = PCA(n_components=2)
X_new = Model.fit_transform(X)
kmeans = KMeans(n_clusters=k,init='k-means++', random_state=0)
y = kmeans.fit(X_new)
labels = kmeans.fit_predict(X_new)
print("\n\nValue by Cluster\n\n")
print_cluster(X_new, labels)
visualize_cluster(X_new, labels)
'''