# Repo per progetto clustering poli

## Dati standardizzati donatori
clustering sui donatori con dati standardizzati

![alt text](images/donor/K2/mean_standard.png)
i dati clusterizzati sembrano essere meglio raggruppati, non più solo per professione. Le media delle altre feature non sembrano più essere identiche
![alt text](images/donor/K3/mean_standard.png)

## Dati standardizzati non donatori
![alt text](images/not_donor/K2/mean_standard.png)
![alt text](images/not_donor/K3/mean_standard.png)

## Dati standardizzati, tutte le osservazioni su domande da 5 a 11
![alt text](images/mean.png)

## Aggiunti file di output excel

## Deviazione standard
![alt text](images/donor/K3/deviazione_standard.png)
![alt text](images/not_donor/K2/deviazione_standard.png)

## Coefficient of variation
![alt text](images/donor/K3/new_coefficient_of_variation.png)
![alt text](images/not_donor/K2/new_coefficient_of_variation.png)
